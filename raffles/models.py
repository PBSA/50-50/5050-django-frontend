import datetime

from django.db import models
from django.utils import timezone
from django.forms import ModelForm



class Organization(models.Model):
    ''' Defines organizations '''

    name = models.CharField(max_length=100, blank=True, null=True)
    address_line1 = models.CharField(max_length=100, blank=True, null=True)
    address_line2 = models.CharField(max_length=100, blank=True, null=True)
    city = models.CharField(max_length=50, blank=True, null=True)
    state = models.CharField(max_length=2, blank=True, null=True)
    country = models.CharField(max_length=2, blank=True, null=True)
    postal_code = models.CharField(max_length=10, blank=True, null=True)
    stripe_id = models.CharField(max_length=25, blank=True, null=True)
    logo = models.ImageField(upload_to = "", blank=True, null=True)   
    website_url = models.CharField(max_length=150, blank=True, null=True)
    non_profit_id = models.IntegerField(blank=True, null=True)

    class Meta:
        verbose_name_plural = "Organizations"
        # ordering = ['order',] # default order is ID

    def __str__(self):
        return self.name



class Raffle(models.Model):
    ''' Defines raffles '''

    image = models.ImageField(upload_to = "raffle/logos/", blank=True, null=True)   
    title = models.CharField("Raffle title", max_length=100, blank=True, null=True)
    name = models.CharField("Raffle name", max_length=100, blank=True, null=True)
    slug = models.SlugField("URL slug", max_length=50, blank=True, null=True)
    description = models.TextField("Raffle description", max_length=500, blank=True, null=True)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE, blank=True, null=True)
    start_date = models.DateTimeField(default=datetime.datetime.now)
    end_date = models.DateTimeField(blank=True, null=True)
    draw_date = models.DateTimeField(blank=True, null=True)
    created_date = models.DateTimeField(auto_now_add=True, blank=True, null=True)
    updated_date = models.DateTimeField(auto_now=True, blank=True, null=True)
    winning_entry = models.ForeignKey('Entry', on_delete=models.PROTECT, related_name="winning_entry", blank=True, null=True)
    # peerplays_draw_id = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Raffles"

    def __str__(self):
        return '{} - {}'.format(self.title, self.name)



class Condition(models.Model):
    ''' Defines conditions required to participate in raffles '''

    raffle = models.ForeignKey(Raffle, on_delete=models.CASCADE)
    description = models.CharField(max_length=200)
    link = models.URLField(max_length=200, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Terms & Conditions"

    def __str__(self):
        return self.description



class TicketBundle(models.Model):
    ''' Defines ticket bundles associated with raffles '''

    quantity = models.IntegerField()
    price = models.DecimalField(max_digits=5, decimal_places=2)
    raffle = models.ForeignKey(Raffle, on_delete=models.CASCADE)

    class Meta:
        verbose_name = "Ticket Bundle"
        verbose_name_plural = "Ticket Bundles"

    def __str__(self):
        return '{} for ${}'.format(self.quantity, self.price)



class Beneficiary(models.Model):
    ''' Defines beneficiaries associated with raffles '''

    name = models.CharField(max_length=100, blank=True, null=True)
    description = models.CharField(max_length=200)
    raffle = models.ForeignKey('Raffle', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Beneficiaries"

    def __str__(self):
        return self.description



class Order(models.Model):
    ''' Stores order details '''

    raffle = models.ForeignKey(Raffle, on_delete=models.CASCADE)
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100) 
    phone = models.CharField(max_length=100)
    bundle = models.ForeignKey(TicketBundle, on_delete=models.CASCADE)
    beneficiary = models.ForeignKey(Beneficiary, on_delete=models.CASCADE, blank=True, null=True)
    payment_method = models.CharField(max_length=25)
    stripe_confirmation = models.CharField(max_length=50, blank=True, null=True)
    seller = models.ForeignKey('Seller', on_delete=models.CASCADE, blank=True, null=True)

    class Meta:
        verbose_name_plural = "Orders"

    def __str__(self):
        return '{} {}'.format(self.pk, self.first_name)


class Entry(models.Model):
    ''' Stores raffle entries '''

    raffle = models.ForeignKey(Raffle, on_delete=models.PROTECT)
    order = models.ForeignKey(Order, on_delete=models.PROTECT, default=0)
    organization = models.ForeignKey(Organization, on_delete=models.PROTECT)
    entry_number = models.IntegerField(default=0)

    class Meta:
        verbose_name_plural = "Entries"
        # db_table = 'app_version'
        constraints = [
            models.UniqueConstraint(fields=['raffle', 'entry_number'], name='unique raffle entry number')
        ]

    def __str__(self):
        return self.id



class Seller(models.Model):

    STATUS = [
        ('active', 'Active'),
        ('inactive', 'Inactive'),
    ]

    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    email = models.EmailField(max_length=100)
    phone = models.CharField(max_length=100)
    authorization_code = models.CharField(max_length=50)
    status = models.CharField(max_length=100, choices=STATUS)
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)

    class Meta: 
        verbose_name_plural = "Sellers"

    def __str__(self):
        return '{} {}'.format(self.first_name, self.last_name)
