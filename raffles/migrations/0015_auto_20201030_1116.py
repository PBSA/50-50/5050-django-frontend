# Generated by Django 3.1.1 on 2020-10-30 14:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('raffles', '0014_auto_20201029_1718'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='order',
            name='organization',
        ),
        migrations.AlterField(
            model_name='order',
            name='seller',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='raffles.seller'),
        ),
    ]
