# Generated by Django 3.1.1 on 2020-11-02 02:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('raffles', '0017_order_beneficiary'),
    ]

    operations = [
        migrations.RenameField(
            model_name='order',
            old_name='stripe',
            new_name='stripe_confirmation',
        ),
    ]
