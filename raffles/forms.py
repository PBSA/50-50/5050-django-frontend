from django import forms
from django.forms import ModelForm
from .models import Order

class OrderForm(ModelForm):
    ''' Raffle entry order form. '''
    
    class Meta:
        model = Order
        fields = ['raffle','first_name','last_name','email','phone','bundle','beneficiary','payment_method','stripe_confirmation','seller']
