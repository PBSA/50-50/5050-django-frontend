from django.urls import path

from . import views

app_name = 'raffles'

urlpatterns = [
    path('thanks/', views.thanks, name='thanks'),
    path('<slug:raffle_slug>/', views.orderform, name='orderform'),
]